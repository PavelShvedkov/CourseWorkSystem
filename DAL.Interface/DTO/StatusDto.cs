namespace DAL.Interface.DTO
{
    public enum StatusDto: byte
    {
        NotAppointed,
        ConfirmationAwaiting,
        Appointed
    }
}