namespace BLL.Interface.Entities
{
    public enum Status: byte
    {
        NotAppointed,
        ConfirmationAwaiting,
        Appointed
    }
}